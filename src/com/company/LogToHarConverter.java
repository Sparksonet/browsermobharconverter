package com.company;


import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class LogToHarConverter {
	public static void main(String[] args) {
		try {
			File file = null;
			String originFolder = null;

			if (!args[0].contains(File.separator)) {	//relative links
				originFolder = System.getProperty("user.dir");
				String filePath = originFolder + File.separator + args[0];
				file = new File(filePath);
			} else {	//absolute links
				originFolder = args[0].substring(0, args[0].lastIndexOf(File.separator));
				file = new File(args[0]);
			}

			long startTime = System.currentTimeMillis();
			Scanner scanner = new Scanner(file);
			String string = scanner.nextLine();
			System.out.println("Original file length = " + string.length());

			string = string.replaceAll("\",\"comment\":\"\"", "\"");
			Files.write(Paths.get(originFolder + File.separator + "output.har"), string.getBytes(), StandardOpenOption.CREATE);

			long endTime = System.currentTimeMillis();
			System.out.println("Output file length = " + string.length());
			System.out.println((endTime - startTime) + " milliseconds");
			System.out.println(originFolder + File.separator +"output.har");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}


